from lab_2.models import Note
from lab_4.forms import NoteForm
from django.http.response import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
def index(request):
    notes = Note.objects.all() 
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context = {}

    # Create object of form
    form = NoteForm(request.POST or None, request.FILES or None)
    
    # Check if form data is valid
    if form.is_valid() and request.method == 'POST':
        # Save the form data to the model
        form.save()
        # Redirect to /lab-3
        return HttpResponseRedirect('/lab-4/')

    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes = Note.objects.all() 
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)