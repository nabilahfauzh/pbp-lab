from django import forms
from django.forms import widgets
from lab_2.models import Note

class MessageInput(forms.Textarea):
    input_type = 'text'

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['receiver', 'sender', 'title', 'message']
        widgets = {
            'message' : MessageInput()
        }
        