from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_1.models import Friend
from lab_3.forms import FriendForm

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
# Sumber: https://www.geeksforgeeks.org/django-modelform-create-form-from-models/
def add_friend(request):
    context = {}

    # Create object of form
    form = FriendForm(request.POST or None, request.FILES or None)
    
    # Check if form data is valid
    if form.is_valid() and request.method == 'POST':
        # Save the form data to the model
        form.save()
        # Redirect to /lab-3
        return HttpResponseRedirect('/lab-3/')

    context['form']= form
    return render(request, "lab3_form.html", context)
