from django import forms
from django.forms import widgets
from lab_1.models import Friend

# Sumber: https://stackoverflow.com/questions/3367091/whats-the-cleanest-simplest-to-get-running-datepicker-in-django
# Show datepicker
class DateInput(forms.DateInput):
    input_type = 'date'

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'dob']
        widgets = {
            'dob' : DateInput()
        } 
